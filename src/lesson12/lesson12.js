import './lesson12.scss';

class LampBlock {    
    constructor(blockElement, disableCallBack) {
        this.toggleBtn = blockElement.querySelector('.lamp-block__btn');        
        this.lamp = blockElement.querySelector('.lamp-block__lamp');        
        this.lampState = false;
        this.disableCallBack = disableCallBack;
        
        this.toggleBtn.addEventListener('click', (e) => {
            if (this.lampState) {
                this.disable();
                if (this.disableCallBack !== undefined) {
                    this.disableCallBack();
                }                
            } else {
                this.enable();
            }
        });
    }

    disable() {
        this.lampState = false;
        this.lamp.classList.remove('lamp-block__lamp_enabled');
        this.toggleBtn.classList.remove('lamp-block__btn_enabled');
    }

    enable() {
        this.lampState = true;
        this.lamp.classList.add('lamp-block__lamp_enabled');
        this.toggleBtn.classList.add('lamp-block__btn_enabled');
    }
}

class Lamps {
    constructor() {        
        this.lampBlokElements = document.querySelectorAll('.lamp-block');
        this.lampBlocks = new Array(this.lampBlokElements.length);
        for(let i = 0; i < this.lampBlocks.length; i ++) {
            this.lampBlocks[i] = new LampBlock(this.lampBlokElements[i], this.lampBlockOnDisableCallBack.bind(this));            
        }

        this.toggleAllBtn = document.querySelector('.lampspanel__btn-toggleAll');
        this.toggleAllBtnSate = false;        
        this.toggleAllBtn.addEventListener('click', (e) => {
            if (this.toggleAllBtnSate) {
                this.toggleAllBtnSate = false;
                this.toggleAllBtn.classList.remove('lampspanel__btn-toggleAll_enabled');
                this.lampBlocks.forEach(lamp => {
                    lamp.disable();
                });
            } else {
                this.toggleAllBtnSate = true;
                this.toggleAllBtn.classList.add('lampspanel__btn-toggleAll_enabled');
                this.lampBlocks.forEach(lamp => {
                    lamp.enable();
                });
            }         
        });
    
    }

    lampBlockOnDisableCallBack() {
        this.toggleAllBtnSate = false;
        this.toggleAllBtn.classList.remove('lampspanel__btn-toggleAll_enabled');
        console.log('lampBlockOnDisableCallBack');
    }
}

let lamps = new Lamps();