import './lesson10.scss';

/* task 1 */
function randomIntager(min, max) {
    if (max <= min) {
        return undefined;
    }

    let diff = max - min;

    return Math.floor(Math.random() * diff + min);
}

console.log(randomIntager(50, 100));

/* task 2 */
let user = {};
user['name'] = 'Вася';
user['surname'] = 'Петров';
user['name'] = 'Андрей';
delete user['name'];

/* task 3 */
function isEmpty(obj) {
    for (let key in obj) {
        return false;
    }

    return true;
}

console.log(isEmpty(user));

/* task 4 */
let salaries = { 'John': 400, 'Jack': 200, 'test': 'asdadast' };
function salaryCalc(obj) {
    let sum = 0;

    for (let key in obj) {
        if (typeof obj[key] === 'number') {
            sum += obj[key];
        }
    }

    return sum;
}

console.log(salaryCalc(salaries));

/* task 5 */
function maxSalaryPerson(obj) {
    let name = 'нет сотрудников';
    let max = undefined;

    for (let key in obj) {
        if (typeof obj[key] === 'number') {
            if (max === undefined) {
                max = obj[key];
                name = key;
                continue;
            }

            if (obj[key] > max) {
                max = obj[key];
                name = key;
            }
        }
    }

    return name;
}

console.log(maxSalaryPerson(salaries));

/* task 6 */
function multiplyNumeric(obj) {
    if (typeof obj[key] === 'number') {
        obj[key] *= 2;
    }
}

/* task 8 */
function aggregator() {
    let numbers = [];
    let sum = 0;

    while (true) {
        let result = prompt('Введите число', 0);
        let num = Number(result);

        if ((result === null) || (Number.isNaN(num))) {
            break;
        }

        numbers.push(num);
    }

    numbers.forEach(n => { sum += n });
    alert(sum);
}
//aggregator();

/* task 9 */
function findVal(arr, val) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === val) {
            return i;
        }
    }
    return -1;
}

console.log(findVal([7, 6, 5, 4, 3, 2, 1], 1));

/* task 10 */
function filterRange(arr, a, b) {
    if ((a >= arr.length) || (b >= arr.length) || (a >= b)) {
        return undefined;
    }

    let result = new Array(b - a);
    let index = 0;

    for (let i = a; i <= b; i++) {
        result[index] = arr[i];
        index++;
    }

    return result;
}

console.log(filterRange([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 3, 6));

/* task 11 */
//Math.pow()
