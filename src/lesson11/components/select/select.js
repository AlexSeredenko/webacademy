import './select.scss';

const SELECT_CLASS_NAME = 'select';
const SELECT_TITLE_CLASS_NAME = `${SELECT_CLASS_NAME}__title`;
const SELECT_OPTIONS_CLASS_NAME = `${SELECT_CLASS_NAME}__options`;
const SELECT_OPTION_CLASS_NAME = `${SELECT_CLASS_NAME}__option`;
const SELECT_CONTROL_CLASS_NAME = `${SELECT_CLASS_NAME}__control`;
const SELECT_EXPAND_CLASS_NAME = `${SELECT_CLASS_NAME}_expant`;

export class Selecet {
    /**
    * @param {HTMLElement} target element where we need to render
    * @param {Array<String>} options for dropdown menu
    */
    constructor(target, options) {
        this.targetElement = target;
        this.options = options;

        this.render();
        this.renderList();
    }

    render() {
        this.titleElement = document.createElement('button');
        this.titleElement.classList.add(SELECT_TITLE_CLASS_NAME);
        this.titleElement.innerText = 'counties';
        this.titleElement.addEventListener('click', () => this.toggle());
        this.targetElement.appendChild(this.titleElement);

        this.listElement = document.createElement('ul');
        this.listElement.classList.add(SELECT_OPTIONS_CLASS_NAME);
        this.targetElement.appendChild(this.listElement);
    }

    renderList() {
        this.options.forEach((option) => {
            const li = document.createElement('li');
            li.classList.add(SELECT_OPTION_CLASS_NAME);
            const button = document.createElement('button');
            button.classList.add(SELECT_CONTROL_CLASS_NAME);
            button.textContent = option;
            li.appendChild(button);
            this.listElement.appendChild(li);
        });
    }

    toggle() {
        this.targetElement.classList.toggle(SELECT_EXPAND_CLASS_NAME);
    }
}