import './bulb.scss';

export class Bulb {
    constructor(targetEl) {
        this.target = targetEl;
        //this.control = targetEl.querySelector('.bulb__control');
        //this.lighter = targetEl.querySelector('.bulb__lighter');
        this.isEnable = false;
        console.log(this);
        this.render();
        this.control.addEventListener('click', this.toggle.bind(this));
    }

    switchOn() {
        this.isEnable = true;
        this.target.classList.add('bulb_active');
    }

    switchOff() {
        this.isEnable = false;
        this.target.classList.remove('bulb_active');
    }

    toggle() {
        if (this.isEnable) {
            this.switchOff();
        } else {
            this.switchOn();
        }
    }

    render() {
        this.control = document.createElement('button');
        this.control.classList.add('bulb__control');
        this.control.innerText = 'toggle';
        this.lighter = document.createElement('div');
        this.lighter.classList.add('bulb__lighter');

        this.target.appendChild(this.lighter);
        this.target.appendChild(this.control);
    }
}