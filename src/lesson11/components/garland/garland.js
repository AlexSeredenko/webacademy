import './garland.scss';
import { Bulb } from '../bulb/bulb';

export class Garland {
    constructor(targetEl) {
        this.targetEl = targetEl;
        this.bulbs = [];
        this.render();
    }

    render() {

        this.targetEl.classList.add('garlnd');

        this.output = document.createElement('div');
        this.output.classList.add('garland__output');
        this.targetEl.appendChild(this.output);

        this.renderControls();
    }

    renderControls() {
        this.controlAdd = document.createElement('button');
        this.controlAdd.addEventListener('click', () => this.add());
        this.controlAdd.classList.add('garland__control');
        this.controlAdd.innerText = 'add';
        this.targetEl.appendChild(this.controlAdd);

        this.controlSwitchOnAll = document.createElement('button');
        this.controlSwitchOnAll.addEventListener('click', () => this.toggleOn());
        this.controlSwitchOnAll.innerText = 'SwitchOnAll';
        this.targetEl.appendChild(this.controlSwitchOnAll);

        this.controlSwitchOffAll = document.createElement('button');
        this.controlSwitchOffAll.addEventListener('click', () => this.toggleOff());
        this.controlSwitchOffAll.innerText = 'SwitchOffAll';
        this.targetEl.appendChild(this.controlSwitchOffAll);
    }

    add() {
        const bulbContainer = document.createElement('div');
        this.bulbs.push(new Bulb(bulbContainer));
        this.output.appendChild(bulbContainer);
    }

    toggleOn() {
        this.bulbs.forEach((bulb) => {
            bulb.switchOn();
        });
    }

    toggleOff() {
        this.bulbs.forEach((bulb) => {
            bulb.switchOff();
        });
    }
}