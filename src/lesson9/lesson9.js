import './lesson9.scss';

function lighter()  {
    let redLight = document.querySelector('#lighterred');
    let yellowLight = document.querySelector('#lighteryellow');
    let greenLight = document.querySelector('#lightergreen');

    function onRedLightBtnclick() {
        redLight.classList.toggle('lighter__redlight_enable');
        yellowLight.className = 'lighter__yellowlight';
        greenLight.className = 'lighter__greenlight';
    }

    function onYellowLightBtnclick() {
        yellowLight.classList.toggle('lighter__yellowlight_enable');
        redLight.className = 'lighter__redlight';        
        greenLight.className = 'lighter__greenlight';
    }

    function onGreenLightBtnclick() {
        greenLight.classList.toggle('lighter__greenlight_enable');
        redLight.className = 'lighter__redlight';
        yellowLight.className = 'lighter__yellowlight';        
    }

    redLight.addEventListener('click', onRedLightBtnclick);
    yellowLight.addEventListener('click', onYellowLightBtnclick);
    greenLight.addEventListener('click', onGreenLightBtnclick);
}

lighter();
