export function dropDownMenu() {    
    let menu = document.querySelector('#mainmenu');
    let toggleButton = document.querySelector('#mainmenuswitcher');

    function toggleBtnOnClick() {               
        menu.classList.toggle('page-nav__menu_col');
        toggleButton.classList.toggle('page-nav__toggle_enable');    
    }

    toggleButton.addEventListener('click', toggleBtnOnClick);
}