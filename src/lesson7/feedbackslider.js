export function feedBackSlider() {
    let btns = document.querySelectorAll('.feedback__data-btn');
    let items = document.querySelectorAll('.feedback__data-item');
    let itemHeaders = document.querySelectorAll('.feedback__data-item-header');
    
    function itemsReset() {
        btns.forEach(b => {b.classList.remove('feedback__data-btn_selected')});
        itemHeaders.forEach(h => {h.classList.remove('feedback__data-item-header_selected')});
        items.forEach(item => {item.classList.remove('feedback__data-item_selected')});
        items.forEach(item => {item.classList.remove('feedback__data-item_num2')});
        items.forEach(item => {item.classList.remove('feedback__data-item_num3')});
    }

    function btn0OnClick() {
        itemsReset();
        
        btns[0].classList.add('feedback__data-btn_selected');
        itemHeaders[0].classList.add('feedback__data-item-header_selected');
        items[0].classList.add('feedback__data-item_selected');
        items[1].classList.add('feedback__data-item_num2');
        items[2].classList.add('feedback__data-item_num3');
    }
    
    function btn1OnClick() {
        itemsReset();
        
        btns[1].classList.add('feedback__data-btn_selected');
        itemHeaders[1].classList.add('feedback__data-item-header_selected');
        items[0].classList.add('feedback__data-item_num3');
        items[1].classList.add('feedback__data-item_selected');
        items[2].classList.add('feedback__data-item_num2');
    }

    function btn2OnClick() {
        itemsReset();
        
        btns[2].classList.add('feedback__data-btn_selected');
        itemHeaders[2].classList.add('feedback__data-item-header_selected');
        items[0].classList.add('feedback__data-item_num2');
        items[1].classList.add('feedback__data-item_num3');
        items[2].classList.add('feedback__data-item_selected');
    }

    function btn3OnClick() {

    }
    
    btns[0].addEventListener('click', btn0OnClick);
    btns[1].addEventListener('click', btn1OnClick);
    btns[2].addEventListener('click', btn2OnClick);
    btns[3].addEventListener('click', btn3OnClick);
    
}